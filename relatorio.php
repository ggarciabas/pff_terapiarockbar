<?php
	include "config.php";

	if(isset($_GET['opcao'])) {
		 $opcao = $_GET['opcao'];
		 if($opcao == 1){	
			header ('Location:home.php?logado');
		 } else if ($opcao == 2) {
			session_start("usuario");
			if (isset($_SESSION["usuario"])) {
				header("Location:produto.php");	
			} else {
				header("Location:index.php");
			}
		 } else if ($opcao == 3) {
			session_start("usuario");
			if (isset($_SESSION["usuario"])) {
				header("Location:categoria.php");	
			} else {
				header("Location:index.php");
			}
		 } else if ($opcao == 4) {
			session_start("usuario");
			if (isset($_SESSION["usuario"])) {
				header("Location:pedido.php");	
			} else {
				header("Location:index.php");
			}
		 } else if ($opcao == 5) {
			session_start("usuario");
			if (isset($_SESSION["usuario"])) {
				header("Location:evento.php");	
			} else {
				header("Location:index.php");
			}
		 } else if ($opcao == 6) {
			session_start("usuario");
			if (isset($_SESSION["usuario"])) {
				header("Location:usuario.php");	
			} else {
				header("Location:index.php");
			}
		 } else if ($opcao == 7) {
			session_start("usuario");
			if (!isset($_SESSION["usuario"])) {
				header("Location:index.php");
			}
		 }
	} 


	

	if (isset($_GET['venda'])) {

		define('FPDF_FONTPATH','font/'); // INSTALA AS FONTES DO FPDF
		require('fpdf.php'); // INSTALA A CLASSE FPDF

		class PDF extends FPDF {// CRIA UMA EXTENSÃO QUE SUBSTITUI AS FUNÇÕES DA CLASSE. SOMENTE AS FUNÇÕES QUE ESTÃO DENTRO DESTE EXTENDS É QUE SERÃO SUBSTITUIDAS.
			
			function Header(){ //CABECALHO
				
				global $codigo; // EXEMPLO DE UMA VARIAVEL QUE TERÁ O MESMO VALOR EM QUALQUER ÁREA DO PDF.

				$l=5; // DEFINI ESTA VARIAVEL PARA ALTURA DA LINHA

				$this->SetXY(10,10); // SetXY -> DEFINE O X E O Y NA PAGINA
				$this->Rect(10,10,190,280); // CRIA UM RETÂNGULO QUE COMEÇA NO X = 10, Y = 10 E TEM 190 DE LARGURA E 280 DE ALTURA. NESTE CASO, É UMA BORDA DE PÁGINA.
				//$this->Image('i.jpg',11,11,40); // INSERE UMA LOGOMARCA NO PONTO X = 11, Y = 11, E DE TAMANHO 40.
				$this->SetFont('Arial','B',8); // DEFINE A FONTE ARIAL, NEGRITO (B), DE TAMANHO 8
				//$this->Cell(170,15,'RELATÓRIO DE CARDÁPIOS',0,1,'L'); // CRIA UMA CELULA COM OS SEGUINTES DADOS, RESPECTIVAMENTE: 
				//$this->Cell(20,$l,'Nº '.$codigo,1,0,'C',0); // CRIA UMA CELULA DA MESMA FORMA ANTERIOR MAS COM ALTURA DEFINIDA PELA VARIAVEL $l E INSERINDO UMA VARIÁVEL NO TEXTO.
				//$this->Cell(20,$l,'Critérios Adotados para notas:',0,1,'L');
				//$this->Cell(200,$l,'Pessimo=1   Ruim=2   Bom=3  Muito Bom=4   Ótimo = 5',0,1,'L');
				// LARGURA = 170, 
				// ALTURA = 15, 
				// TEXTO = 'INSIRA SEU TEXTO AQUI'
				// BORDA = 0. SE = 1 TEM BORDA SE 'R' = RIGTH, 'L' = LEFT, 'T' = TOP, 'B' = BOTTOM
				// QUEBRAR LINHA NO FINAL = 0 = NÃO
				// ALINHAMENTO = 'L' = LEFT
				
				//$this->Ln(); // QUEBRA DE LINHA

				$this->Cell(190,10,'',0,0,'L');
			
				$l = 17;
				$this->SetFont('Arial','B',12);
				$this->SetXY(10,15);
				$this->Cell(190,$l,'RELATORIO DE PRODUTOS VENDIDOS','B',1,'C');
	//			$this->Ln();
				
				$l=5;

				$this->SetFont('Arial','B',10);
				$this->Cell(20,$l,"DATA ".date('d/m/Y'),'B',0,'L'); // INSIRO A DATA CORRENTE NA CELULA
				
				$this->Ln();

				//FINAL DO CABECALHO COM DADOS

				//ABAIXO É CRIADO O TITULO DA TABELA DE DADOS
				$this->Cell(190,2,'',0,0,'C');//ESPAÇAMENTO DO CABECALHO PARA A TABELA
				$this->Ln();
				$this->SetTextColor(255,255,255);
				$this->Cell(190,$l,'',1,0,'C',1);
				$this->Ln();

				//TITULO DA TABELA DE SERVIÇOS
				$this->SetFillColor(232,232,232);
				$this->SetTextColor(0,0,0);
				$this->SetFont('Arial','B',8);
				$this->Cell(50,$l,'Produto',1,0,'L',1);
				$this->Cell(20,$l,'Total vendido',1,0,'l',1);
				$this->Ln();
			}

			function Footer(){ // CRIANDO UM RODAPE
			}

		}


		$pdf = new PDF('P','mm','A4'); //CRIA UM NOVO ARQUIVO PDF NO TAMANHO A4
		$pdf->AddPage(); // ADICIONA UMA PAGINA
		$pdf->AliasNbPages(); // SELECIONA O NUMERO TOTAL DE PAGINAS, USADO NO RODAPE

		$pdf->SetFont('Arial','',8);
		$y = 50; // AQUI EU COLOCO O Y INICIAL DOS DADOS 
		$l=5; // ALTURA DA LINHA

		$CONEXAO=mysql_pconnect($servidor_bd, $usuario_bd, $senha_bd) or die (mysql_error()); // conecta com o banco de dados
		mysql_select_db($banco_bd, $CONEXAO); // seleciona o banco a ser utilizado
		$query = sprintf("SELECT * FROM produto");
		$dados = mysql_query($query, $CONEXAO) or die (mysql_error()); // sql
		$linha = mysql_fetch_assoc($dados);	
		$total = mysql_num_rows($dados);

		do {

			$proId = $linha['id'];

			$queryItem = sprintf("
						SELECT * FROM item WHERE produto = $proId
				");

			$dadosItem = mysql_query($queryItem) or die (mysql_error());
			$linhaItem = mysql_fetch_assoc($dadosItem);	
			$totalItem = mysql_num_rows($dadosItem);
			if ($totalItem != 0) {
				$TOTALITEM = 0;
				//$TOTALQT = 0;

				do {

					$TOTALITEM += $linhaItem['quantidade'] * $linhaItem['preco'];
					//$TOTALQT += $linhaItem['quantidade'];

				} while ($linhaItem = mysql_fetch_assoc($dadosItem));


				$l = $l; // A FUNÇÃO contaLinhas CONTA QUANTAS LINHAS UM CAMPO TEM, UTILIZANDO A LARGURA 48.
				if($y+$l>=230){ // 230 É O TAMANHO MAXIMO ANTES DO RODAPE
					$pdf->AddPage(); // SE ULTRAPASSADO, É ADICIONADO UMA PÁGINA
					$y=59; // E O Y INICIAL É RESETADO
				}

				$prodDescricao = $linha['descricao'];

				//DADOS*/
				$pdf->SetY($y);
				$pdf->SetX(10);
				$pdf->Rect(10,$y,70,$l);
				$pdf->MultiCell(31,6,$prodDescricao,0,2);
				$pdf->SetY($y);
				$pdf->SetX(60);
				$pdf->Rect(10,$y,70,$l);
				$pdf->MultiCell(31,6,"R$ ".sprintf ("%.2f", $TOTALITEM),0,2); // ESTA É A CELULA QUE PODE TER DADOS EM MAIS DE UMA LINHA
			
				$y += $l;
			}

		} while ($linha = mysql_fetch_assoc($dados));

		$pdf->Output(); // IMPRIME O PDF NA TELA
		mysqli_close($CONEXAO); // FECHA A CONEXÃO COM MYSQL
		Header('Pragma: public'); // ESTA FUNÇÃO É USADA PELO FPDF PARA PUBLICAR NO IE
	
	} else if (isset($_GET['pedido'])) {

		define('FPDF_FONTPATH','font/'); // INSTALA AS FONTES DO FPDF
		require('fpdf.php'); // INSTALA A CLASSE FPDF

		class PDF extends FPDF {// CRIA UMA EXTENSÃO QUE SUBSTITUI AS FUNÇÕES DA CLASSE. SOMENTE AS FUNÇÕES QUE ESTÃO DENTRO DESTE EXTENDS É QUE SERÃO SUBSTITUIDAS.
			
			function Header(){ //CABECALHO
				
				global $codigo; // EXEMPLO DE UMA VARIAVEL QUE TERÁ O MESMO VALOR EM QUALQUER ÁREA DO PDF.

				$l=5; // DEFINI ESTA VARIAVEL PARA ALTURA DA LINHA

				$this->SetXY(10,10); // SetXY -> DEFINE O X E O Y NA PAGINA
				$this->Rect(10,10,190,280); // CRIA UM RETÂNGULO QUE COMEÇA NO X = 10, Y = 10 E TEM 190 DE LARGURA E 280 DE ALTURA. NESTE CASO, É UMA BORDA DE PÁGINA.
				//$this->Image('i.jpg',11,11,40); // INSERE UMA LOGOMARCA NO PONTO X = 11, Y = 11, E DE TAMANHO 40.
				$this->SetFont('Arial','B',8); // DEFINE A FONTE ARIAL, NEGRITO (B), DE TAMANHO 8
				//$this->Cell(170,15,'RELATÓRIO DE CARDÁPIOS',0,1,'L'); // CRIA UMA CELULA COM OS SEGUINTES DADOS, RESPECTIVAMENTE: 
				//$this->Cell(20,$l,'Nº '.$codigo,1,0,'C',0); // CRIA UMA CELULA DA MESMA FORMA ANTERIOR MAS COM ALTURA DEFINIDA PELA VARIAVEL $l E INSERINDO UMA VARIÁVEL NO TEXTO.
				$this->Cell(20,$l,'Critérios Adotados para notas:',0,1,'L');
				$this->Cell(200,$l,'Pessimo=1   Ruim=2   Bom=3  Muito Bom=4   Ótimo = 5',0,1,'L');
				// LARGURA = 170, 
				// ALTURA = 15, 
				// TEXTO = 'INSIRA SEU TEXTO AQUI'
				// BORDA = 0. SE = 1 TEM BORDA SE 'R' = RIGTH, 'L' = LEFT, 'T' = TOP, 'B' = BOTTOM
				// QUEBRAR LINHA NO FINAL = 0 = NÃO
				// ALINHAMENTO = 'L' = LEFT
				
				//$this->Ln(); // QUEBRA DE LINHA

				$this->Cell(190,10,'',0,0,'L');
			
				$l = 17;
				$this->SetFont('Arial','B',12);
				$this->SetXY(10,15);
				$this->Cell(190,$l,'RELATÓRIOS','B',1,'C');
	//			$this->Ln();
				
				$l=5;

				$this->SetFont('Arial','B',10);
				/*$this->Cell(20,$l,'Dados 1:',0,0,'L');
				$this->Cell(100,$l,'','B',0,'L');
				$this->Cell(35,$l,'',0,0,'L');
				$this->Cell(15,$l,'Data:',0,0,'L');*/
				$this->Cell(20,$l,date('d/m/Y'),'B',0,'L'); // INSIRO A DATA CORRENTE NA CELULA
				
				/*$this->Ln();
				$this->Cell(20,$l,'Dados 2:',0,0,'L');
				$this->Cell(100,$l,'','B',0,'L');
				$this->Ln();
				$this->Cell(20,$l,'Dados 3:',0,0,'L');
				$this->Cell(100,$l,'','B',0,'L');
				$this->Cell(35,$l,'',0,0,'L');
				$this->Cell(15,$l,'Dados 4:',0,0,'L');
				$this->Cell(20,$l,'','B',0,'L');*/
				$this->Ln();

				//FINAL DO CABECALHO COM DADOS

				//ABAIXO É CRIADO O TITULO DA TABELA DE DADOS
				$this->Cell(190,2,'',0,0,'C');//ESPAÇAMENTO DO CABECALHO PARA A TABELA
				$this->Ln();
				$this->SetTextColor(255,255,255);
				$this->Cell(190,$l,'',1,0,'C',1);
				$this->Ln();

				//TITULO DA TABELA DE SERVIÇOS
				$this->SetFillColor(232,232,232);
				$this->SetTextColor(0,0,0);
				$this->SetFont('Arial','B',8);
				$this->Cell(50,$l,'CARDÁPIO',1,0,'L',1);
				$this->Cell(20,$l,'MÉDIA',1,0,'l',1);
				/*$this->Cell(70,$l,'Titulo 3',1,0,'L',1);
				$this->Cell(12,$l,'Titulo 4',1,0,'C',1);
				$this->Cell(12,$l,'Titulo 5',1,0,'C',1);
				$this->Cell(40,$l,'Titulo 6',1,0,'C',1);
				$this->Cell(15,$l,'Titulo 7',1,0,'C',1);*/
				$this->Ln();
			}

			function Footer(){ // CRIANDO UM RODAPE
	/*
				$this->SetXY(15,280);
				$this->Rect(10,270,190,20);
				$this->SetFont('Arial','',10);
				$this->Cell(70,8,'Assinatura ','T',0,'L');
				$this->Cell(40,8,' ',0,0,'L');
				$this->Cell(70,8,'Assinatura','T',0,'L'); 
				$this->Ln();
				$this->SetFont('Arial','',7);
				$this->Cell(190,7,'Página '.$this->PageNo().' de {nb}',0,0,'C');*/
			}

		}


		$pdf = new PDF('P','mm','A4'); //CRIA UM NOVO ARQUIVO PDF NO TAMANHO A4
		$pdf->AddPage(); // ADICIONA UMA PAGINA
		$pdf->AliasNbPages(); // SELECIONA O NUMERO TOTAL DE PAGINAS, USADO NO RODAPE

		$pdf->SetFont('Arial','',8);
		$y = 50; // AQUI EU COLOCO O Y INICIAL DOS DADOS 
		$l=5; // ALTURA DA LINHA

		$CONEXAO=mysql_pconnect($servidor_bd, $usuario_bd, $senha_bd) or die (mysql_error()); // conecta com o banco de dados
		mysql_select_db($banco_bd, $CONEXAO); // seleciona o banco a ser utilizado
		$query = sprintf("SELECT * FROM pedido");
		$dados = mysql_query($query, $CONEXAO) or die (mysql_error()); // sql
		$linha = mysql_fetch_assoc($dados);	
		$total = mysql_num_rows($dados);

		$query = sprintf("SELECT * FROM pedido");

		while($row = mysql_fetch_array($result)){ // ENQUANTO OS DADOS VÃO PASSANDO, O FPDF VAI INSERINDO OS DADOS NA PAGINA

			$dados1 = utf8_decode($row["CAR_DESCRICAO"]);
			$dados = utf8_decode($row["AVG(VOT_MEDIA)"]); // NESTE CASO, EU DECODIFIQUEI OS DADOS, POIS É UM CAMPO DE TEXTO.
			$dados2 = number_format ($dados, 2);
			$l = $l; // A FUNÇÃO contaLinhas CONTA QUANTAS LINHAS UM CAMPO TEM, UTILIZANDO A LARGURA 48.
			if($y+$l>=230){ // 230 É O TAMANHO MAXIMO ANTES DO RODAPE
				$pdf->AddPage(); // SE ULTRAPASSADO, É ADICIONADO UMA PÁGINA
				$y=59; // E O Y INICIAL É RESETADO
			}

			//DADOS*/
			$pdf->SetY($y);
			$pdf->SetX(10);
			$pdf->Rect(10,$y,70,$l);
			$pdf->MultiCell(31,6,$dados1,0,2);
			$pdf->SetY($y);
			$pdf->SetX(60);
			$pdf->Rect(10,$y,70,$l);
			$pdf->MultiCell(31,6,$dados2,0,2); // ESTA É A CELULA QUE PODE TER DADOS EM MAIS DE UMA LINHA
		
			$y += $l;
		}

		$pdf->Output(); // IMPRIME O PDF NA TELA
		mysqli_close($CONEXAO); // FECHA A CONEXÃO COM MYSQL
		Header('Pragma: public'); // ESTA FUNÇÃO É USADA PELO FPDF PARA PUBLICAR NO IE
		

	} else if ($_GET['evento']) {

		define('FPDF_FONTPATH','font/'); // INSTALA AS FONTES DO FPDF
		require('fpdf.php'); // INSTALA A CLASSE FPDF

		class PDF extends FPDF {// CRIA UMA EXTENSÃO QUE SUBSTITUI AS FUNÇÕES DA CLASSE. SOMENTE AS FUNÇÕES QUE ESTÃO DENTRO DESTE EXTENDS É QUE SERÃO SUBSTITUIDAS.
			
			function Header(){ //CABECALHO
				
				global $codigo; // EXEMPLO DE UMA VARIAVEL QUE TERÁ O MESMO VALOR EM QUALQUER ÁREA DO PDF.

				$l=5; // DEFINI ESTA VARIAVEL PARA ALTURA DA LINHA

				$this->SetXY(10,10); // SetXY -> DEFINE O X E O Y NA PAGINA
				$this->Rect(10,10,190,280); // CRIA UM RETÂNGULO QUE COMEÇA NO X = 10, Y = 10 E TEM 190 DE LARGURA E 280 DE ALTURA. NESTE CASO, É UMA BORDA DE PÁGINA.
				//$this->Image('i.jpg',11,11,40); // INSERE UMA LOGOMARCA NO PONTO X = 11, Y = 11, E DE TAMANHO 40.
				$this->SetFont('Arial','B',8); // DEFINE A FONTE ARIAL, NEGRITO (B), DE TAMANHO 8
				//$this->Cell(170,15,'RELATÓRIO DE CARDÁPIOS',0,1,'L'); // CRIA UMA CELULA COM OS SEGUINTES DADOS, RESPECTIVAMENTE: 
				//$this->Cell(20,$l,'Nº '.$codigo,1,0,'C',0); // CRIA UMA CELULA DA MESMA FORMA ANTERIOR MAS COM ALTURA DEFINIDA PELA VARIAVEL $l E INSERINDO UMA VARIÁVEL NO TEXTO.
				$this->Cell(20,$l,'Critérios Adotados para notas:',0,1,'L');
				$this->Cell(200,$l,'Pessimo=1   Ruim=2   Bom=3  Muito Bom=4   Ótimo = 5',0,1,'L');
				// LARGURA = 170, 
				// ALTURA = 15, 
				// TEXTO = 'INSIRA SEU TEXTO AQUI'
				// BORDA = 0. SE = 1 TEM BORDA SE 'R' = RIGTH, 'L' = LEFT, 'T' = TOP, 'B' = BOTTOM
				// QUEBRAR LINHA NO FINAL = 0 = NÃO
				// ALINHAMENTO = 'L' = LEFT
				
				//$this->Ln(); // QUEBRA DE LINHA

				$this->Cell(190,10,'',0,0,'L');
			
				$l = 17;
				$this->SetFont('Arial','B',12);
				$this->SetXY(10,15);
				$this->Cell(190,$l,'RELATÓRIOS','B',1,'C');
	//			$this->Ln();
				
				$l=5;

				$this->SetFont('Arial','B',10);
				/*$this->Cell(20,$l,'Dados 1:',0,0,'L');
				$this->Cell(100,$l,'','B',0,'L');
				$this->Cell(35,$l,'',0,0,'L');
				$this->Cell(15,$l,'Data:',0,0,'L');*/
				$this->Cell(20,$l,date('d/m/Y'),'B',0,'L'); // INSIRO A DATA CORRENTE NA CELULA
				
				/*$this->Ln();
				$this->Cell(20,$l,'Dados 2:',0,0,'L');
				$this->Cell(100,$l,'','B',0,'L');
				$this->Ln();
				$this->Cell(20,$l,'Dados 3:',0,0,'L');
				$this->Cell(100,$l,'','B',0,'L');
				$this->Cell(35,$l,'',0,0,'L');
				$this->Cell(15,$l,'Dados 4:',0,0,'L');
				$this->Cell(20,$l,'','B',0,'L');*/
				$this->Ln();

				//FINAL DO CABECALHO COM DADOS

				//ABAIXO É CRIADO O TITULO DA TABELA DE DADOS
				$this->Cell(190,2,'',0,0,'C');//ESPAÇAMENTO DO CABECALHO PARA A TABELA
				$this->Ln();
				$this->SetTextColor(255,255,255);
				$this->Cell(190,$l,'',1,0,'C',1);
				$this->Ln();

				//TITULO DA TABELA DE SERVIÇOS
				$this->SetFillColor(232,232,232);
				$this->SetTextColor(0,0,0);
				$this->SetFont('Arial','B',8);
				$this->Cell(50,$l,'CARDÁPIO',1,0,'L',1);
				$this->Cell(20,$l,'MÉDIA',1,0,'l',1);
				/*$this->Cell(70,$l,'Titulo 3',1,0,'L',1);
				$this->Cell(12,$l,'Titulo 4',1,0,'C',1);
				$this->Cell(12,$l,'Titulo 5',1,0,'C',1);
				$this->Cell(40,$l,'Titulo 6',1,0,'C',1);
				$this->Cell(15,$l,'Titulo 7',1,0,'C',1);*/
				$this->Ln();
			}

			function Footer(){ // CRIANDO UM RODAPE
	/*
				$this->SetXY(15,280);
				$this->Rect(10,270,190,20);
				$this->SetFont('Arial','',10);
				$this->Cell(70,8,'Assinatura ','T',0,'L');
				$this->Cell(40,8,' ',0,0,'L');
				$this->Cell(70,8,'Assinatura','T',0,'L'); 
				$this->Ln();
				$this->SetFont('Arial','',7);
				$this->Cell(190,7,'Página '.$this->PageNo().' de {nb}',0,0,'C');*/
			}

		}


		$pdf = new PDF('P','mm','A4'); //CRIA UM NOVO ARQUIVO PDF NO TAMANHO A4
		$pdf->AddPage(); // ADICIONA UMA PAGINA
		$pdf->AliasNbPages(); // SELECIONA O NUMERO TOTAL DE PAGINAS, USADO NO RODAPE

		$pdf->SetFont('Arial','',8);
		$y = 50; // AQUI EU COLOCO O Y INICIAL DOS DADOS 
		$l=5; // ALTURA DA LINHA

		$CONEXAO=mysql_pconnect($servidor_bd, $usuario_bd, $senha_bd) or die (mysql_error()); // conecta com o banco de dados
		mysql_select_db($banco_bd, $CONEXAO); // seleciona o banco a ser utilizado
		$query = sprintf("SELECT * FROM pedido");
		$dados = mysql_query($query, $CONEXAO) or die (mysql_error()); // sql
		$linha = mysql_fetch_assoc($dados);	
		$total = mysql_num_rows($dados);

		$query = sprintf("SELECT * FROM pedido");

		while($row = mysql_fetch_array($result)){ // ENQUANTO OS DADOS VÃO PASSANDO, O FPDF VAI INSERINDO OS DADOS NA PAGINA

			$dados1 = utf8_decode($row["CAR_DESCRICAO"]);
			$dados = utf8_decode($row["AVG(VOT_MEDIA)"]); // NESTE CASO, EU DECODIFIQUEI OS DADOS, POIS É UM CAMPO DE TEXTO.
			$dados2 = number_format ($dados, 2);
			$l = $l; // A FUNÇÃO contaLinhas CONTA QUANTAS LINHAS UM CAMPO TEM, UTILIZANDO A LARGURA 48.
			if($y+$l>=230){ // 230 É O TAMANHO MAXIMO ANTES DO RODAPE
				$pdf->AddPage(); // SE ULTRAPASSADO, É ADICIONADO UMA PÁGINA
				$y=59; // E O Y INICIAL É RESETADO
			}

			//DADOS*/
			$pdf->SetY($y);
			$pdf->SetX(10);
			$pdf->Rect(10,$y,70,$l);
			$pdf->MultiCell(31,6,$dados1,0,2);
			$pdf->SetY($y);
			$pdf->SetX(60);
			$pdf->Rect(10,$y,70,$l);
			$pdf->MultiCell(31,6,$dados2,0,2); // ESTA É A CELULA QUE PODE TER DADOS EM MAIS DE UMA LINHA
		
			$y += $l;
		}

		$pdf->Output(); // IMPRIME O PDF NA TELA
		mysqli_close($CONEXAO); // FECHA A CONEXÃO COM MYSQL
		Header('Pragma: public'); // ESTA FUNÇÃO É USADA PELO FPDF PARA PUBLICAR NO IE

	}

?>

<!doctype html>  
   <head>
   <meta charset="UTF-8">
   <title>Relat&oacute;rio - Terapia RockBar</title>
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
     <![endif]-->

   <link rel="stylesheet" type="text/css" href="css/styles.css"/>
   </head>
   <body>

   <!--start container-->
   <div id="container">

   <!--start header-->
   <header>

   <!--start logo-->
   <a href="index.php" id="logo"><img src="images/logo.png" width="221" height="84" alt="logo"/></a>    
   <!--end logo-->

   <!--start menu-->

   <nav>
	   <ul>
		   <li><a href="categoria.php?opcao=1">In&iacute;cio</a></li>
	   <li><a href="produto.php?opcao=2">Produtos</a></li>
	   <li><a href="produto.php?opcao=3">Categorias</a></li>
	   <li><a href="produto.php?opcao=4">Pedidos</a></li>
	   <li><a href="produto.php?opcao=5">Eventos</a></li>
	   <li><a href="produto.php?opcao=6">Usuarios</a></li>
	   <li><a href="produto.php?opcao=7" class="current">Relat&oacute;rios</a></li>
	   </ul>
   </nav>
   <!--end menu-->

   <!--end header-->
   </header>

   <!--start holder-->

   <div class="holder_content">

   <section class="produto">
   <h3>Relat&oacute;rios</h3>
   	<p>Você tem as seguintes opções para relat&oacute;rio: </p>

   <section class="login">
		<a href="relatorio.php?venda"> Produtos vendidos </a><br/>
		<a href="relatorio.php?produto"> Produto estoque </a><br/>
		<a href="relatorio.php?cardapio"> Cardápio </a>
   </section>

   </section>

   </div>
   <!--end holder-->

   </div>
   <!--end container-->

   <!--start footer-->
   <footer>
   <div class="container">  
   <div id="FooterTwo"> © 2013 </div>
  <div id="FooterTree"> Desenvolvido por: Giovanna Garcia, Bruna Magalhaes, Gian Fritsche e Cassiano Peres</div> 
   </div>
   </div>
   </footer>
   <!--end footer-->  
   </body>
</html>
