<?php
	include "config.php";
	if(isset($_GET['opcao'])) {
		 $opcao = $_GET['opcao'];
		 if($opcao == 1){	
			header ('Location:home.php?logado');
		 } else if ($opcao == 2) {
			session_start("usuario");
			if (!isset($_SESSION["usuario"])) {
				header("Location:index.php");
			}
		 } else if ($opcao == 3) {
			session_start("usuario");
			if (isset($_SESSION["usuario"])) {
				header("Location:categoria.php");	
			} else {
				header("Location:index.php");
			}
		 } else if ($opcao == 4) {
			session_start("usuario");
			if (isset($_SESSION["usuario"])) {
				header("Location:pedido.php");	
			} else {
				header("Location:index.php");
			}
		 } else if ($opcao == 5) {
			session_start("usuario");
			if (isset($_SESSION["usuario"])) {
				header("Location:evento.php");	
			} else {
				header("Location:index.php");
			}
		 } else if ($opcao == 6) {
			session_start("usuario");
			if (isset($_SESSION["usuario"])) {
				header("Location:usuario.php");	
			} else {
				header("Location:index.php");
			}
		 } else if ($opcao == 7) {
			session_start("usuario");
			if (isset($_SESSION["usuario"])) {
				header("Location:relatorio.php");	
			} else {
				header("Location:index.php");
			}
		 }
	} 
?>

<!doctype html>  
   <head>
   <meta charset="UTF-8">
   <title>Produto - Terapia RockBar</title>
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
     <![endif]-->

   <link rel="stylesheet" type="text/css" href="css/styles.css"/>
   </head>
   <body>

   <!--start container-->
   <div id="container">

   <!--start header-->
   <header>

   <!--start logo-->
   <a href="index.php" id="logo"><img src="images/logo.png" width="221" height="84" alt="logo"/></a>    
   <!--end logo-->

   <!--start menu-->

   <nav>
	   <ul>
	   <li><a href="categoria.php?opcao=1">In&iacute;cio</a></li>
	   <li><a href="produto.php?opcao=2" class="current">Produtos</a></li>
	   <li><a href="produto.php?opcao=3">Categorias</a></li>
	   <li><a href="produto.php?opcao=4">Pedidos</a></li>
	   <li><a href="produto.php?opcao=5">Eventos</a></li>
	   <li><a href="produto.php?opcao=6">Usuarios</a></li>
	   <li><a href="produto.php?opcao=7"> Relat&oacute;rios</a></li>
	   	   </ul>
   </nav>
   <!--end menu-->

   <!--end header-->
   </header>

   <!--start holder-->

   <div class="holder_content">

   <section class="produto">
   <h3>Consulta de Produto</h3>
   	<p>Produtos cadastrados no sistema::: </p>

  <section class="group1">
	<? 
		$CONEXAO=mysql_pconnect($servidor_bd, $usuario_bd, $senha_bd) or die (mysql_error()); // conecta com o banco de dados
		mysql_select_db($banco_bd, $CONEXAO); // seleciona o banco a ser utilizado
		$query = sprintf("SELECT * FROM produto");
		$dados = mysql_query($query, $CONEXAO) or die (mysql_error()); // sql
		$linha = mysql_fetch_assoc($dados);	
		$total = mysql_num_rows($dados);
		echo "<table align='center' class='gridtable consulta'>
				<tr>
					<th>Id</th>
					<th>Descricao</th>
					<th>Preco</td>
					<th>Categoria</th>
					<th>Quantidade</th>
				</tr>";
		if ($total != 0) {			
			// imprime todos os dados da tabela		
			do {
				$id = $linha['id'];
				$descricao = $linha['descricao'];
				$preco = $linha['preco'];
				$categoria = $linha['categoria'];
				$quantidade = $linha['quantidade'];
				if ($quantidade == 0) {
					echo "<tr style='background-color:#FF3D28'>";
				} else {
					echo "<tr>";
				}
				echo "<td>".$id."</td>
					<td class='grande'>".$descricao."</td>
					<td class='centro'>R$ ".sprintf("%.2f",$preco)."</td>
					<td>";

					// pesquisando categoria

					$queryCat = sprintf("SELECT * FROM categoria WHERE id = $categoria");
					$dadosCat = mysql_query($queryCat, $CONEXAO) or die (mysql_error()); // sql
					$linha = mysql_fetch_assoc($dadosCat);

					echo $linha['descricao'];


				echo "</td>	
					<td class='centro'>".$quantidade."</td>	
				      </tr>";
			} while($linha = mysql_fetch_assoc($dados));
						
		}
		echo "</table>";
		mysql_close($CONEXAO);
	?>
	<br/><br/><br/><br/>
   </section>

   <aside class="group2">  
   <h3>Opções</h3>
	<article class="holder_news">
		<a href="produto_cad.php">Adicionar</a><br/>
		<a href="produto_ed.php">Editar</a><br/>
		<a href="produto_est.php">Inserir estoque</a><br/>
		<a href="produto_cons.php">Consulta</a>
	   </section>
   </aside>

   </div>
   <!--end holder-->

   </div>
   <!--end container-->

   <!--start footer-->
   <footer>
   <div class="container">  
   <div id="FooterTwo"> © 2013 </div>
  <div id="FooterTree"> Desenvolvido por: Giovanna Garcia, Bruna Magalhaes, Gian Fritsche e Cassiano Peres</div> 
   </div>
   </div>
   </footer>
   <!--end footer-->  
   </body>
</html>
