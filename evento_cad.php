<?php
	include "config.php";
	if(isset($_GET['opcao'])) {
		 $opcao = $_GET['opcao'];
		 if($opcao == 1){	
			header ('Location:home.php?logado');
		 } else if ($opcao == 2) {
			session_start("usuario");
			if (isset($_SESSION["usuario"])) {
				header("Location:produto.php");	
			} else {
				header("Location:index.php");
			}
		 } else if ($opcao == 3) {
			session_start("usuario");
			if (isset($_SESSION["usuario"])) {
				header("Location:categoria.php");	
			} else {
				header("Location:index.php");
			}
		 } else if ($opcao == 4) {
			session_start("usuario");
			if (isset($_SESSION["usuario"])) {
				header("Location:pedido.php");	
			} else {
				header("Location:index.php");
			}
		 } else if ($opcao == 5) {
			session_start("usuario");
			if (!isset($_SESSION["usuario"])) {
				header("Location:index.php");
			}
		 } else if ($opcao == 6) {
			session_start("usuario");
			if (isset($_SESSION["usuario"])) {
				header("Location:usuario.php");	
			} else {
				header("Location:index.php");
			}
		 } else if ($opcao == 7) {
			session_start("usuario");
			if (isset($_SESSION["usuario"])) {
				header("Location:relatorio.php");	
			} else {
				header("Location:index.php");
			}
		 }
	} 

	if (isset($_GET['cadastro'])) {
		// efetuar o cadastro do evento no banco
		$DESCRICAO = $_POST['descricao'];
		$DATA	= $_POST['datepicker'];
		$OBSERVACAO = $_POST['obs'];

		$DATA = strtotime( $DATA );
		$DATA = date( 'Y-m-d', $DATA );

		$CONEXAO=mysql_pconnect($servidor_bd, $usuario_bd, $senha_bd) or die (mysql_error()); // conecta com o banco de dados
		mysql_select_db($banco_bd, $CONEXAO); // seleciona o banco a ser utilizado
		$query = sprintf("INSERT INTO evento ( descricao, data, observacao) VALUES ('$DESCRICAO', '$DATA', '$OBSERVACAO')");
		$dados = mysql_query($query, $CONEXAO) or die (mysql_error()); // sql	
		mysql_close($CONEXAO);
		// retornar para evento.
		//header("Location:evento.php");	

	}

?>

<!doctype html>  
   <head>
   <meta charset="UTF-8">
   <title>Evento - Terapia RockBar</title>
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
     <![endif]-->

  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
  <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
  <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script>
  $(function() {
    $( "#datepicker" ).datepicker();
  });
  </script>
  
   <link rel="stylesheet" type="text/css" href="css/styles.css"/>
   </head>
   <body>

   <!--start container-->
   <div id="container">

   <!--start header-->
   <header>

   <!--start logo-->
   <a href="index.php" id="logo"><img src="images/logo.png" width="221" height="84" alt="logo"/></a>    
   <!--end logo-->

   <!--start menu-->

   <nav>
	   <ul>
	   <li><a href="categoria.php?opcao=1">In&iacute;cio</a></li>
	   <li><a href="evento.php?opcao=2">Produtos</a></li>
	   <li><a href="evento.php?opcao=3">Categorias</a></li>	
	   <li><a href="evento.php?opcao=4">Pedidos</a></li>
	   <li><a href="evento.php?opcao=5" class="current">Eventos</a></li>
	   <li><a href="evento.php?opcao=6">Usuarios</a></li>
	   <li><a href="produto.php?opcao=7"> Relat&oacute;rios</a></li>
	   </ul>
   </nav>
   <!--end menu-->

   <!--end header-->
   </header>

   <!--start holder-->

   <div class="holder_content">

   <section class="evento">
   <h3>Cadastro de evento</h3>
   	<p>Cadastro de evento:::</p>

   <section class="group1">
	<form id="form" name="form" action="evento_cad.php?cadastro" method="post" >
	   <table>
	    <tr>
	      <td><label> Descricao: </label></td>
	      <td><input type="text" id="descricao" name="descricao" value="" /></td>
            </tr>
	    <tr>
	      <td><label> Data: </label></td>
	      <td><input type="text" id="datepicker" name="datepicker"/></td>
            </tr>
	    <tr>
              <td><label> Observacao: </label></td>
              <td><input type="text" id="obs" name="obs" value="" /></td>
            </tr>
            <tr>
               <td></td>
	       <td><input type="submit" value="Cadastrar" /></td>
	    </tr>	                        
	  </table>
        </form>
   </section>

   </section>

   <aside class="group2">  
   <h3>Opções</h3>
	<article class="holder_news">
		<a href="evento_cad.php">Adicionar</a><br/>
		<a href="evento_ed.php">Editar</a><br/>
		<a href="evento_cons.php">Consultar</a>
	   </section>
   </aside>

   </div>
   <!--end holder-->

   </div>
   <!--end container-->

   <!--start footer-->
   <footer>
   <div class="container">  
   <div id="FooterTwo"> © 2013 </div>
  <div id="FooterTree"> Desenvolvido por: Giovanna Garcia, Bruna Magalhaes, Gian Fritsche e Cassiano Peres</div> 
   </div>
   </div>
   </footer>
   <!--end footer-->  
   </body>
</html>
