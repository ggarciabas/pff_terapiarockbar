<?php
	if(isset($_GET['opcao'])) {
		 $opcao = $_GET['opcao'];
		 if($opcao == 1){	
			session_start("usuario");
			if (!isset($_SESSION["usuario"])) {
				header("Location:index.php");
			} else {
				header("Location:home.php?logado");
			}
		 } else if ($opcao == 2) {
			session_start("usuario");
			if (isset($_SESSION["usuario"])) {
				header("Location:produto.php");	
			} else {
				header("Location:index.php");
			}
		 } else if ($opcao == 3) {
			session_start("usuario");
			if (isset($_SESSION["usuario"])) {
				header("Location:categoria.php");	
			} else {
				header("Location:index.php");
			}
		 } else if ($opcao == 4) {
			session_start("usuario");
			if (isset($_SESSION["usuario"])) {
				header("Location:pedido.php");	
			} else {
				header("Location:index.php");
			}
		 } else if ($opcao == 5) {
			session_start("usuario");
			if (isset($_SESSION["usuario"])) {
				header("Location:evento.php");	
			} else {
				header("Location:index.php");
			}
		 } else if ($opcao == 6) {
			session_start("usuario");
			if (isset($_SESSION["usuario"])) {
				header("Location:usuario.php");	
			} else {
				header("Location:index.php");
			}
		 } else if ($opcao == 7) {
			session_start("usuario");
			if (isset($_SESSION["usuario"])) {
				header("Location:relatorio.php");	
			} else {
				header("Location:index.php");
			}
		 }
	} 

	if (isset($_GET['logado'])) {
		session_start("usuario");
		if (isset($_SESSION["usuario"])) {
			$servidor_bd="localhost";
			$usuario_bd="root";
			$senha_bd="12345";
			$banco_bd="terapia";
			$CONEXAO=mysql_pconnect($servidor_bd, $usuario_bd, $senha_bd) or die (mysql_error()); // conecta com o banco de dados
			mysql_select_db($banco_bd, $CONEXAO); // seleciona o banco a ser utilizado
			$USER = $_SESSION["usuario"];
			$SENHA = $_SESSION["senha"];
			$query = sprintf("SELECT * FROM usuario where login = '$USER' and senha = '$SENHA'");
			$dados = mysql_query($query, $CONEXAO) or die (mysql_error()); // sql
			$linha = mysql_fetch_assoc($dados);
			$USUARIO = $linha['nome'];
		} else {
			header("Location:index.php");
		}
	}

?>

<!doctype html>  
   <head>
   <meta charset="UTF-8">
   <title>Home - Terapia RockBar</title>
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
     <![endif]-->

   <link rel="stylesheet" type="text/css" href="css/styles.css"/>
   </head>
   <body>

   <!--start container-->
   <div id="container">

   <!--start header-->
   <header>

   <!--start logo-->
   <a href="index.php" id="logo"><img src="images/logo.png" width="221" height="84" alt="logo"/></a>    
   <!--end logo-->

   <!--start menu-->

   <nav>
	   <ul>
	   <li><a href="home.php?opcao=1" class="current">In&iacute;cio</a></li>
	   <li><a href="home.php?opcao=2">Produtos</a></li>
	   <li><a href="home.php?opcao=3">Categorias</a></li>	
	   <li><a href="home.php?opcao=4">Pedidos</a></li>
	   <li><a href="home.php?opcao=5">Eventos</a></li>
	   <li><a href="home.php?opcao=6">Usuarios</a></li>
	   <li><a href="produto.php?opcao=7"> Relat&oacute;rios</a></li>
	   </ul>
   </nav>
   <!--end menu-->

   <!--end header-->
   </header>

   <!--start holder-->

   <div class="holder_content">

   <section class="home">
   <h3>Home</h3>
   	<p>Olá, <? echo $USUARIO; ?>.</p>

   <section class="login">
	Nesta WebApp voc&ecirc; poder&aacute; consultar, editar e adicionar produtos, categorias de
	produtos, pedidos, eventos e usu&aacute;rios do sistema.
	Poder&aacute; tamb&eacute;m visualizar relat&oacute;rios.
	Esta WebApp foi desenvolvida no &acirc;mbito da disciplina de Projeto e Desenvolvimento de Sistemas em Ambiente Internet.
	Do curso de T&eacute;cnologia em An&aacute;lise e Desenvolvimento de Sistemas da Universidade Tecnol&oacute;gica Federal do Paran&aacute;.
	Pelos alunos Bruna Magalh&atilde;es, Cassiano Peres, Gian Fritsche e Giovanna Garcia.
   </section>

   </section>

   </div>
   <!--end holder-->

   </div>
   <!--end container-->

   <!--start footer-->
   <footer>
   <div class="container">  
   <div id="FooterTwo"> © 2013 </div>
  <div id="FooterTree"> Desenvolvido por: Giovanna Garcia, Bruna Magalhaes, Gian Fritsche e Cassiano Peres</div> 
   </div>
   </div>
   </footer>
   <!--end footer-->  
   </body>
</html>
