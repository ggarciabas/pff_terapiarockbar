create database terapia;

use terapia;

create table categoria (id int primary key AUTO_INCREMENT, descricao varchar(25));

create table produto (id int primary key AUTO_INCREMENT, descricao varchar(25), preco double, categoria int, quantidade int, foreign key (categoria) references categoria (id));

create table usuario (id int primary key AUTO_INCREMENT, nome varchar(80), login varchar(20), senha varchar(20));

create table evento (id int primary key AUTO_INCREMENT, descricao varchar(100), data date, observacao varchar(200));

create table pedido (id int primary key AUTO_INCREMENT, descricao varchar(50), observacao varchar(200));

create table item ( produto int, pedido int, quantidade int, preco double, primary key (produto, pedido), foreign key (produto) references produto(id), foreign key (pedido) references pedido(id));

insert into usuario (nome, login, senha) values ('Administrador', 'admin', 'admin');
