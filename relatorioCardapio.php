
<?php

	header("Content-Type: text/html; charset=utf8_unicode_ci");
	
	///variaveis passadas por post.
	$data = $_POST['data'];
	$tipo = $_POST['tipo'];	

	if(empty($data)){
		echo "<script>
				alert('Favor digitar a data !!!!!');
				window.close();
			</script>";
	}else{
		
		////extrair dia, mes e ano da data passada pelo usuario
		$ex = explode("-", $data);
		$ano = $ex[0];
		$mes = $ex[1];
		$dia = $ex[2];
			
		define('FPDF_FONTPATH','font/'); // INSTALA AS FONTES DO FPDF
		require('fpdf.php'); // INSTALA A CLASSE FPDF

		class PDF extends FPDF {// CRIA UMA EXTENS�O QUE SUBSTITUI AS FUN��ES DA CLASSE. SOMENTE AS FUN��ES QUE EST�O DENTRO DESTE EXTENDS � QUE SER�O SUBSTITUIDAS.
			
			function Header(){ //CABECALHO
				
				global $codigo; // EXEMPLO DE UMA VARIAVEL QUE TER� O MESMO VALOR EM QUALQUER �REA DO PDF.

				$l=5; // DEFINI ESTA VARIAVEL PARA ALTURA DA LINHA

				$this->SetXY(10,10); // SetXY -> DEFINE O X E O Y NA PAGINA
				$this->Rect(10,10,190,280); // CRIA UM RET�NGULO QUE COME�A NO X = 10, Y = 10 E TEM 190 DE LARGURA E 280 DE ALTURA. NESTE CASO, � UMA BORDA DE P�GINA.
				//$this->Image('i.jpg',11,11,40); // INSERE UMA LOGOMARCA NO PONTO X = 11, Y = 11, E DE TAMANHO 40.
				$this->SetFont('Arial','B',8); // DEFINE A FONTE ARIAL, NEGRITO (B), DE TAMANHO 8
				//$this->Cell(170,15,'RELAT�RIO DE CARD�PIOS',0,1,'L'); // CRIA UMA CELULA COM OS SEGUINTES DADOS, RESPECTIVAMENTE: 
				//$this->Cell(20,$l,'N� '.$codigo,1,0,'C',0); // CRIA UMA CELULA DA MESMA FORMA ANTERIOR MAS COM ALTURA DEFINIDA PELA VARIAVEL $l E INSERINDO UMA VARI�VEL NO TEXTO.
				$this->Cell(20,$l,'Crit�rios Adotados para notas:',0,1,'L');
				$this->Cell(200,$l,'Pessimo=1   Ruim=2   Bom=3  Muito Bom=4   �timo = 5',0,1,'L');
				// LARGURA = 170, 
				// ALTURA = 15, 
				// TEXTO = 'INSIRA SEU TEXTO AQUI'
				// BORDA = 0. SE = 1 TEM BORDA SE 'R' = RIGTH, 'L' = LEFT, 'T' = TOP, 'B' = BOTTOM
				// QUEBRAR LINHA NO FINAL = 0 = N�O
				// ALINHAMENTO = 'L' = LEFT

				
				//$this->Ln(); // QUEBRA DE LINHA

				$this->Cell(190,10,'',0,0,'L');
			

				$l = 17;
				$this->SetFont('Arial','B',12);
				$this->SetXY(10,15);
				$this->Cell(190,$l,'RELAT�RIOS','B',1,'C');
	//			$this->Ln();
				
				$l=5;

				$this->SetFont('Arial','B',10);
				/*$this->Cell(20,$l,'Dados 1:',0,0,'L');
				$this->Cell(100,$l,'','B',0,'L');
				$this->Cell(35,$l,'',0,0,'L');
				$this->Cell(15,$l,'Data:',0,0,'L');*/
				$this->Cell(20,$l,date('d/m/Y'),'B',0,'L'); // INSIRO A DATA CORRENTE NA CELULA
				
				/*$this->Ln();
				$this->Cell(20,$l,'Dados 2:',0,0,'L');
				$this->Cell(100,$l,'','B',0,'L');
				$this->Ln();
				$this->Cell(20,$l,'Dados 3:',0,0,'L');
				$this->Cell(100,$l,'','B',0,'L');
				$this->Cell(35,$l,'',0,0,'L');
				$this->Cell(15,$l,'Dados 4:',0,0,'L');
				$this->Cell(20,$l,'','B',0,'L');*/
				$this->Ln();

				//FINAL DO CABECALHO COM DADOS

				//ABAIXO � CRIADO O TITULO DA TABELA DE DADOS
				$this->Cell(190,2,'',0,0,'C');//ESPA�AMENTO DO CABECALHO PARA A TABELA
				$this->Ln();
				$this->SetTextColor(255,255,255);
				$this->Cell(190,$l,'',1,0,'C',1);
				$this->Ln();

				//TITULO DA TABELA DE SERVI�OS
				$this->SetFillColor(232,232,232);
				$this->SetTextColor(0,0,0);
				$this->SetFont('Arial','B',8);
				$this->Cell(50,$l,'CARD�PIO',1,0,'L',1);
				$this->Cell(20,$l,'M�DIA',1,0,'l',1);
				/*$this->Cell(70,$l,'Titulo 3',1,0,'L',1);
				$this->Cell(12,$l,'Titulo 4',1,0,'C',1);
				$this->Cell(12,$l,'Titulo 5',1,0,'C',1);
				$this->Cell(40,$l,'Titulo 6',1,0,'C',1);
				$this->Cell(15,$l,'Titulo 7',1,0,'C',1);*/
				$this->Ln();
			}

			function Footer(){ // CRIANDO UM RODAPE
	/*
				$this->SetXY(15,280);
				$this->Rect(10,270,190,20);
				$this->SetFont('Arial','',10);
				$this->Cell(70,8,'Assinatura ','T',0,'L');
				$this->Cell(40,8,' ',0,0,'L');
				$this->Cell(70,8,'Assinatura','T',0,'L'); 
				$this->Ln();
				$this->SetFont('Arial','',7);
				$this->Cell(190,7,'P�gina '.$this->PageNo().' de {nb}',0,0,'C');*/
			}
		}

		//CONECTE SE AO BANCO DE DADOS SE PRECISAR 
		//CONECTA COM O MYSQL
		//$dbc = mysqli_connect('localhost','ru','r1st0r5nt3','ru') or die('Error Connecting to Mysql server.');
		//$dbc = mysqli_connect('localhost','root','','ru') or die('Error Connecting to Mysql server.');
		 //connection com o banco mysql (wampp) 
		require_once ('../conexao/conexao.class.php');
		require_once ('../crud.class.php');
    
		$dbc = new conexao(); // instancia classe de conxao
		$dbc->connect(); // abre conexao com o banco
		
		$pdf = new PDF('P','mm','A4'); //CRIA UM NOVO ARQUIVO PDF NO TAMANHO A4
		$pdf->AddPage(); // ADICIONA UMA PAGINA
		$pdf->AliasNbPages(); // SELECIONA O NUMERO TOTAL DE PAGINAS, USADO NO RODAPE

		$pdf->SetFont('Arial','',8);
		$y = 50; // AQUI EU COLOCO O Y INICIAL DOS DADOS 

		
			if($tipo == "dia"){
			
				$result = mysql_query("SELECT CAR_DESCRICAO, AVG(VOT_MEDIA) 
						FROM TB_CARDAPIO, TB_REFEICAO, TB_VOTACAO
						WHERE CAR_CODIGO = REF_CODCARDAPIO
						AND REF_CODIGO = VOT_CODREFEICAO
						AND VOT_DATA LIKE '$data'
						GROUP BY CAR_DESCRICAO");		
			}else{
				if($tipo == 'mes'){
					$result = mysql_query("SELECT CAR_DESCRICAO, AVG(VOT_MEDIA)
						FROM TB_CARDAPIO, TB_REFEICAO, TB_VOTACAO
						WHERE CAR_CODIGO = REF_CODCARDAPIO
						AND REF_CODIGO = VOT_CODREFEICAO
						AND EXTRACT(MONTH FROM REF_DATA) = EXTRACT(MONTH FROM '$data') 
						GROUP BY CAR_DESCRICAO");						
				}else{
					if($tipo == 'ano'){
						$result = mysql_query("SELECT CAR_DESCRICAO, AVG(VOT_MEDIA)
							FROM TB_CARDAPIO, TB_REFEICAO, TB_VOTACAO
							WHERE CAR_CODIGO = REF_CODCARDAPIO
							AND REF_CODIGO = VOT_CODREFEICAO
							AND EXTRACT(YEAR FROM REF_DATA) = EXTRACT(YEAR FROM '$data') 
							GROUP BY CAR_DESCRICAO");	
					}
				}		
			}								
		$l=5; // ALTURA DA LINHA
		while($row = mysql_fetch_array($result)){ // ENQUANTO OS DADOS V�O PASSANDO, O FPDF VAI INSERINDO OS DADOS NA PAGINA

			$dados1 = utf8_decode($row["CAR_DESCRICAO"]);
			$dados = utf8_decode($row["AVG(VOT_MEDIA)"]); // NESTE CASO, EU DECODIFIQUEI OS DADOS, POIS � UM CAMPO DE TEXTO.
			$dados2 = number_format ($dados, 2);
			$l = $l; // A FUN��O contaLinhas CONTA QUANTAS LINHAS UM CAMPO TEM, UTILIZANDO A LARGURA 48.
			if($y+$l>=230){ // 230 � O TAMANHO MAXIMO ANTES DO RODAPE
				$pdf->AddPage(); // SE ULTRAPASSADO, � ADICIONADO UMA P�GINA
				$y=59; // E O Y INICIAL � RESETADO
			}

			//DADOS*/
			$pdf->SetY($y);
			$pdf->SetX(10);
			$pdf->Rect(10,$y,70,$l);
			$pdf->MultiCell(31,6,$dados1,0,2);
			$pdf->SetY($y);
			$pdf->SetX(60);
			$pdf->Rect(10,$y,70,$l);
			$pdf->MultiCell(31,6,$dados2,0,2); // ESTA � A CELULA QUE PODE TER DADOS EM MAIS DE UMA LINHA
		
			$y += $l;
		}
		
	//mysqli_close(); // FECHA A CONEX�O COM MYSQL

	$pdf->Output(); // IMPRIME O PDF NA TELA
	Header('Pragma: public'); // ESTA FUN��O � USADA PELO FPDF PARA PUBLICAR NO IE
	}

?>