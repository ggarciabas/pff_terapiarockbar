<?php
	include "config.php";
	if(isset($_GET['opcao'])) {
		 $opcao = $_GET['opcao'];
		 if($opcao == 1){	
			header ('Location:home.php?logado');
		 } else if ($opcao == 2) {
			session_start("usuario");
			if (isset($_SESSION["usuario"])) {
				header("Location:produto.php");	
			} else {
				header("Location:index.php");
			}
		 } else if ($opcao == 4) {
			session_start("usuario");
			if (isset($_SESSION["usuario"])) {
				header("Location:pedido.php");	
			} else {
				header("Location:index.php");
			}
		 } else if ($opcao == 5) {
			session_start("usuario");
			if (isset($_SESSION["usuario"])) {
				header("Location:evento.php");	
			} else {
				header("Location:index.php");
			}
		 } else if ($opcao == 6) {
			session_start("usuario");
			if (!isset($_SESSION["usuario"])) {
				header("Location:index.php");
			}
		 } else if ($opcao == 7) {
			session_start("usuario");
			if (isset($_SESSION["usuario"])) {
				header("Location:relatorio.php");	
			} else {
				header("Location:index.php");
			}
		 }
	} 
?>

<!doctype html>  
   <head>
   <meta charset="UTF-8">
   <title>Usuario - Terapia RockBar</title>
   <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
     <![endif]-->

   <link rel="stylesheet" type="text/css" href="css/styles.css"/>
   </head>
   <body>

   <!--start container-->
   <div id="container">

   <!--start header-->
   <header>

   <!--start logo-->
   <a href="index.php" id="logo"><img src="images/logo.png" width="221" height="84" alt="logo"/></a>    
   <!--end logo-->

   <!--start menu-->

   <nav>
	   <ul>
	  	   <li><a href="categoria.php?opcao=1">In&iacute;cio</a></li>
	   <li><a href="usuario.php?opcao=2">Produtos</a></li>
	   <li><a href="usuario.php?opcao=3">Categorias</a></li>	
	   <li><a href="usuario.php?opcao=4">Pedidos</a></li>
	   <li><a href="usuario.php?opcao=5">Eventos</a></li>
	   <li><a href="usuario.php?opcao=6" class="current">Usuarios</a></li>
	   <li><a href="produto.php?opcao=7"> Relat&oacute;rios</a></li>
	   </ul>
   </nav>
   <!--end menu-->

   <!--end header-->
   </header>

   <!--start holder-->

   <div class="holder_content">

   <section class="usuario">
   <h3>Consulta de Usuarios</h3>
   	<p>Usuarios cadastrados no sistema::: </p>

   <section class="group1">
	<? 
		$CONEXAO=mysql_pconnect($servidor_bd, $usuario_bd, $senha_bd) or die (mysql_error()); // conecta com o banco de dados
		mysql_select_db($banco_bd, $CONEXAO); // seleciona o banco a ser utilizado
		$query = sprintf("SELECT * FROM usuario");
		$dados = mysql_query($query, $CONEXAO) or die (mysql_error()); // sql
		$linha = mysql_fetch_assoc($dados);	
		$total = mysql_num_rows($dados);
		echo "<table class='gridtable consulta' align='center'>
			<tr>
				<th>Id</th>
				<th>Nome</th>
				<th>Login</th>
			</tr>";
		// imprime todos os dados da tabela		
		do {
			$id = $linha['id'];
			$nome = $linha['nome'];
			$login = $linha['login'];
			echo "<tr>
				<td>".$id."</td>
				<td class='grande'>".$nome."</td>
				<td class='grande'>".$login."</td>
			      </tr>";
		} while($linha = mysql_fetch_assoc($dados));
		echo "</table>";
		mysql_close($CONEXAO);
	?>
   </section>

   </section>

   <aside class="group2">  
   <h3>Opções</h3>
	<article class="holder_news">
		<a href="usuario_cad.php">Adicionar</a><br/>
		<a href="usuario_ed.php">Editar</a><br/>
		<a href="usuario_cons.php">Consulta</a>
	   </section>
   </aside>

   </div>
   <!--end holder-->

   </div>
   <!--end container-->

   <!--start footer-->
   <footer>
   <div class="container">  
   <div id="FooterTwo"> © 2013 </div>
  <div id="FooterTree"> Desenvolvido por: Giovanna Garcia, Bruna Magalhaes, Gian Fritsche e Cassiano Peres</div> 
   </div> 
   </div>
   </footer>
   <!--end footer-->  
   </body>
</html>
