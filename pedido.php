<?php
	include "config.php";
	if(isset($_GET['opcao'])) {
		 $opcao = $_GET['opcao'];
		 if($opcao == 1){	
			header ('Location:home.php?logado');
		 } else if ($opcao == 2) {
			session_start("usuario");
			if (isset($_SESSION["usuario"])) {
				header("Location:produto.php");	
			} else {
				header("Location:index.php");
			}
		 } else if ($opcao == 3) {
			session_start("usuario");
			if (isset($_SESSION["usuario"])) {
				header("Location:categoria.php");	
			} else {
				header("Location:index.php");
			}
		 } else if ($opcao == 4) {
			session_start("usuario");
			if (!isset($_SESSION["usuario"])) {
				header("Location:index.php");
			}
		 } else if ($opcao == 5) {
			session_start("usuario");
			if (isset($_SESSION["usuario"])) {
				header("Location:evento.php");	
			} else {
				header("Location:index.php");
			}
		 } else if ($opcao == 6) {
			session_start("usuario");
			if (isset($_SESSION["usuario"])) {
				header("Location:usuario.php");	
			} else {
				header("Location:index.php");
			}
		 } else if ($opcao == 7) {
			session_start("usuario");
			if (isset($_SESSION["usuario"])) {
				header("Location:relatorio.php");	
			} else {
				header("Location:index.php");
			}
		 }
	} 

?>

<!doctype html>  
   <head>
   <meta charset="UTF-8">
   <title>Pedido - Terapia RockBar</title>
     <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
     <![endif]-->

   <link rel="stylesheet" type="text/css" href="css/styles.css"/>
   </head>
   <body>

   <!--start container-->
   <div id="container">

   <!--start header-->
   <header>

   <!--start logo-->
   <a href="index.php" id="logo"><img src="images/logo.png" width="221" height="84" alt="logo"/></a>    
   <!--end logo-->

   <!--start menu-->

   <nav>
	   <ul>
	   <li><a href="categoria.php?opcao=1">In&iacute;cio</a></li>
	   <li><a href="pedido.php?opcao=2">Produtos</a></li>
	   <li><a href="pedido.php?opcao=3">Categorias</a></li>
	   <li><a href="pedido.php?opcao=4" class="current">Pedidos</a></li>
	   <li><a href="pedido.php?opcao=5">Eventos</a></li>
	   <li><a href="pedido.php?opcao=6">Usuarios</a></li>
	   <li><a href="produto.php?opcao=7"> Relat&oacute;rios</a></li>
	   </ul>
   </nav>
   <!--end menu-->

   <!--end header-->
   </header>

   <!--start holder-->

   <div class="holder_content">

   <section class="pedido">
   <h3>Consulta de Pedidos</h3>
   	<p>Pedidos cadastrados no sistema::: </p>

   <section class="group1">
	<? 		
		$CONEXAO=mysql_pconnect($servidor_bd, $usuario_bd, $senha_bd) or die (mysql_error()); // conecta com o banco de dados
		mysql_select_db($banco_bd, $CONEXAO); // seleciona o banco a ser utilizado
		$query = sprintf("SELECT * FROM pedido");
		$dados = mysql_query($query, $CONEXAO) or die (mysql_error()); // sql
		$linha = mysql_fetch_assoc($dados);	
		$total = mysql_num_rows($dados);
		echo "<table align='center' class='gridtable consulta'>
			<tr>
				<th>Id</th>
				<th>Descricao</th>
				<th>Observacao</th>
				<th>Itens</th>
				<th>Total conta</th>
			</tr>";
		// imprime todos os dados da tabela		
		do {
			$id = $linha['id'];
			$descricao = $linha['descricao'];
			$observacao = $linha['observacao'];
			echo "<tr>
				<td>".$id."</td>
				<td class='grande'>".$descricao."</td>
				<td class='grande'>".$observacao."</td>
				<td>";

			$idItem = $linha['id'];
			$query2 = sprintf("SELECT * FROM item WHERE pedido = '$idItem'");
			$dados2 = mysql_query($query2, $CONEXAO) or die (mysql_error()); // sql
			$linha2 = mysql_fetch_assoc($dados2);	
			$total2 = mysql_num_rows($dados2);

			$totalConsumido = 0;
			echo "<table align='center' class='gridtable2'><tr><th>Id</th><th>Descricao</th><th>Preco</th><th>Quantidade</th><th>Total</th></tr>";
			if ($total2 > 0) {
				
				do {
					$idProduto = $linha2['produto'];
					$query3 = sprintf("SELECT * FROM produto WHERE id = $idProduto");
					$dados3 = mysql_query($query3, $CONEXAO) or die (mysql_error()); // sql
					$linha3 = mysql_fetch_assoc($dados3);	
					$total3 = mysql_num_rows($dados3);
					$totalConsumido += $linha2['quantidade']*$linha2['preco'];
					echo "<tr>
						<td>".$linha3['id']."</td>
						<td class='grande'>".$linha3['descricao']."</td>
						<td class='centro'> R$ ".$linha2['preco']."</td>
						<td>".$linha2['quantidade']."</td>
						<td class='centro'> R$ ".sprintf("%.2f", $linha2['quantidade']*$linha2['preco'])."</td>
					     </tr>";		
				} while ($linha2 = mysql_fetch_assoc($dados2));

			}
			echo "</table>";		
			echo "</td>
				<td class='centro'> R$ ".sprintf("%.2f", $totalConsumido)."</td>
			     </tr>";
		} while($linha = mysql_fetch_assoc($dados));
		echo "</table>";
		mysql_close($CONEXAO);
	?>
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
   </section>

   </section>

   <aside class="group2">  
   <h3>Opções</h3>
	<article class="holder_news">
		<a href="pedido_cad.php?novo">Novo</a><br/>
		<!-- <a href="pedido_ad.php">Adicionar itens ao pedido</a><br/>
		<a href="pedido_rem.php">Remover itens do pedido</a><br/>	-->
		<a href="pedido_ed.php">Editar</a><br/>
		<a href="pedido_cons.php">Consultar</a><br/>
	   </section>
   </aside>

   </div>
   <!--end holder-->

   </div>
   <!--end container-->

   <!--start footer-->
   <footer>
   <div class="container">  
   <div id="FooterTwo"> © 2013 </div>
  <div id="FooterTree"> Desenvolvido por: Giovanna Garcia, Bruna Magalhaes, Gian Fritsche e Cassiano Peres</div> 
   </div>
   </div>
   </footer>
   <!--end footer-->  
   </body>
</html>
